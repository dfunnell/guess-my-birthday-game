from random import randint

name = input('Hi, Whats your name? ')

guess = 1
month = randint(1,12)
year = randint(1924,2004)

for guess in range(1,6):
    month = randint(1,12)
    year = randint(1924,2004)

    print(f'Guess {guess} : {name} were you born in {month} / {year}? ')

    response = input('yes or no? ').lower()

    if response == 'yes':
        print('I knew it!')
        exit()
    elif guess == 5:
        print('I have other things to do. Good bye.')
    else:
        print('Drat! Lemme try again!')
